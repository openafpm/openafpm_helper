(function ($) {

Drupal.behaviors.chart_select = {
  attach: function (context, settings) {
    $('#edit-chart-type').change(function() {
    	chart_type = $(this).val()
    	var nid = Drupal.settings.ntua_wind.nid;
    	console.log(chart_type)
      Highcharts.charts[0].showLoading()

    	$.getJSON( "/ajax/chart_data/"+nid+"/"+chart_type, function(data) {
        // console.log( "success" );
          console.log(data);
          while(Highcharts.charts[0].series.length > 0)
            Highcharts.charts[0].series[0].remove(true);
          Highcharts.charts[0].update({
            title: { text: data.chart_title },
            subtitle: { text: data.subtitle },
            tooltip: {
              shared: false,
              headerFormat: ''
            }
          })
          // Highcharts.charts[0].setTitle({ text: data.chart_title });
          // Highcharts.charts[0].setSubtitle({ text: data.subtitle });
          Highcharts.charts[0].xAxis[0].setTitle({ text: data.xAxis.title})
          Highcharts.charts[0].yAxis[0].setTitle({ text: data.yAxis.title})
          for (var series of data.series) {
            // console.log(series);
            Highcharts.charts[0].addSeries({
              name: series.title,
              data: series.data,
              showInLegend: series.showInLegend,
              color: series.color,
              marker: {
                  enabled: series.showPoints
              },
              tooltip: {
                  pointFormat: series.ttFormat
              },
            })
          }
      })
		  .done(function() {
        Highcharts.charts[0].hideLoading()
		    // console.log( "second success" );
		  })
		  .fail(function() {
		    // console.log( "error" );
        alert('There was an error while fetching chart data')
		  })
		  .always(function() {
		    // console.log( "complete" );
		  });
    })

    $('#edit-chart-type').prop('selectedIndex', 0).change();

  }
};

}(jQuery));
