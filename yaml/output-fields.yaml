﻿Blade Rotor Outputs:
  n_cut_in:
    Name: RPM at the cut-in wind speed (TSR)
    Description: RPM at the cut-in wind speed based on the input TSR
    Units: RPM

  # n_inst:
  #   Name: RPM at the instantaneous wind speed
  #   Description: RPM at the instantaneous wind speed
  #   Units: RPM

  n_nom:
    Name: RPM at the nominal wind speed (TSR)
    Description: RPM at the nominal wind speed based on the input TSR
    Units: RPM

  tsr_nom:
    Name: Nominal tip speed ratio
    Description: Tip speed ratio at the nominal wind speed
    Units:

  cutin_RPM:
    Name: RPM at the cut-in wind speed (Flux Linkage)
    Description: The RPM at the cut-in wind speed using simulated values of flux linkage from FEMM
    Units: RPM

  cutin_Voltage:
    Name: Cut-in Voltage (Flux Linkage)
    Description: The battery voltage at cut-in using simulated values of flux linkage from FEMM
    Units: V

  cutin_Vw:
    Name: Wind speed at cut-in RPM (Flux Linkage)
    Description: The wind speed at the cut-in RPM using simulated values of flux linkage from FEMM
    Units: m/s

  # N_max:
  #   Name: Maximum RPM
  #   Description: The maximum RPM the simulation was run for
  #   Units: RPM

  Pmech_Torque_x:
    Name: Mechanical power (Torque)
    Description: The mechanical power delivered to the generator by the blade rotor at the nominal wind speed using simulated values of force from FEMM
    Units: W

  # Vw_inst:
  #   Name: Instantaneous wind speed
  #   Description: The instantaneous wind speed is the wind speed at which 10% more power that the nominal power at 10m/s is produced
  #   Units: m/s

Generator Rotor Outputs:
   hr:
    Name: Rotor disk thickness 
    Description: The thickness of the rotor disk
    Units: mm
   ai:
    Name: Magnet width-to-pole pitch ratio
    Description: The ratio of the magnet width to the distance between the starting points of two consecutive magnets
    Units:

   kd:
    Name: Inner-to-outer magnet radius
    Description: The ratio of the Inner-to-outer magnet radius
    Units:

   Bmax:
    Name: Flux density in airgap
    Description: Magnitude of the normal flux density in the middle of the airgap, ie in the middle of the coil
    Units: T

   Bmg:
    Name: Flux density on magnet surface
    Description: Magnitude of the normal flux density on the surface of the magnet
    Units: T

   Bp:
    Name: First harmonic of flux density in airgap
    Description: Magnitude of the first harmonic of the normal flux density in the middle of the airgap, ie in the middle of the coil
    Units: T

   Flux_pole:
    Name: Flux per pole (Calculated)
    Description: The calculated flux per pole taking into account changes in ai and kd using analytical formulas
    Units: Wb

   # Block_Energy_per_Volume:
   #  Name: Magnetic field energy
   #  Description: The energy in the magnetic field of the generator
   #  Units: J/m3

   #
   # Max_EperV:
   #  Name: Maximum energy per volume
   #  Description: Maximum energy per volume for the optimal airgap
   #  Units: J/m3

   #
   # g_max_energy:
   #  Name: Maximum energy mechanical clearance
   #  Description: The mechanical clearance for the airgap with the maximum energy magnetic field
   #  Units: mm

   #
   # Optimal_airgap:
   #  Name: Optimal airgap
   #  Description: The axial distance between opposing magnets for the maximum magnetic field energy in the generator
   #  Units: mm

   magnet_num:
    Name: Magnets on a rotor disk
    Description: The number of magnets on a rotor disk
    Units:

   dist_magnet_Rin:
    Name: Distance between magnets
    Description: The distance between two consecutive magnets at the inner radius
    Units: mm

   MagnetMass:
    Name: Magnet mass
    Description: Total mass of all magnets on rotors
    Units: g

   mag_density:
    Name: Magnet density
    Description: Density of the magnetic material
    Units: g/cm3

   Vmag:
    Name: Volume of a single magnet
    Description: Volume of a single magnet
    Units: mm3

   VolumeMagnet:
    Name: Volume of all magnets
    Description: The volume all magnets on rotors
    Units: mm3

   Br:
    Name: Remanent magnetic flux density
    Description: Remanent magnetic flux density
    Units: T

   Hc:
    Name: Magnetic coercivity
    Description: Magnetic coercivity
    Units: A/m

  BHmax:
   Name: Maximum Energy Product
   Description: Maximum Energy Product
   Units: KJ/m³

  Dout:
    Name: Outer diameter
    Description: The outer diameter of the rotor disks of the generator
    Units: mm

  Ravg:
    Name: Average radius
    Description: The average radius of the effective length of the generator, which is the same as the middle of the magnet length in the radial direction
    Units: mm

  Rin:
    Name: Inner radius
    Description: The inner radius of the effective length of the generator
    Units: mm

  Rout:
    Name: Outer radius
    Description: The outer radius of the rotor disks of the generator
    Units: mm

  Rout_eff:
    Name: Effective outer radius
    Description: The outer radius of the effective length of the generator
    Units: mm

  BackIronMass:
    Name: Mass of rotor disks
    Description: The mass of the rotor disks without the mass of the magnets
    Units: kgr

  BackIronVolume:
    Name: Volume of rotor disks
    Description: The volume of the rotor disks
    Units: cm3

  SaturMAX:
    Name: Maximum flux density in the magnet rotor disk
    Description: Maximum flux density in the middle of the magnet rotor disk thickness
    Units: T

  Saturation:
    Name: Is the magnet rotor disk saturated?
    Description: Is the magnet rotor disk saturated?
    Units:

  la_out:
    Name: Magnet length,
    Description: The magnet length for each time the optimization was repeated
    Units: mm
  wm_out:
    Name: Magnet width,
    Description: The magnet width for each time the optimization was repeated
    Units: mm
  hm_out:
    Name: Magnet thickness,
    Description: The magnet thickness for each time the optimization was repeated
    Units: mm

Generator Stator Outputs:
  tw:
    Name: Stator thickness
    Description: The stator thickness
    Units: mm
  tw_out:
    Name: Stator thickness
    Description: The stator thickness for each time the optimization was repeated
    Units: mm
  nphase:
    Name: Number of phases
    Description: The number of phases in the stator – use default value of 3
    Units:

  q:
    Name: Number of coils per phase
    Description: Number of coils per phase
    Units:

  coil_num:
    Name: Total number of coils in stator
    Description: Total number of coils in stator
    Units:

  Nc:
    Name: Number of turns per coil
    Description: Number of turns per coil
    Units:

  dc:
    Name: Copper wire diameter
    Description: The diameter of the copper conductor in the coils
    Units: mm

  No_wires_at_hand:
    Name: Number of wires at hand
    Description: The number of wires at hand when winding a coil
    Units:
  kf:
    Name: Winding fill factor
    Description: The area occupied by copper over the cross-sectional area of the coil leg. This value will depend on how tight the coils can be wound
    Units:

   # dc_two_wires:
   #  Name: 2-wire copper diameter
   #  Description: The copper wire diameter of the coils using two parallel wires at hand
   #  Units: mm

  sc:
    Name: Copper wire cross-sectional area
    Description: The cross-sectional area of the copper conductor used in the coils based on commercial wire sizes
    Units: mm2

  sc_theor:
    Name: Copper wire cross-sectional area (Calculated)
    Description: The calculated cross-sectional area of the copper conductor to be used in the coils
    Units: mm²

  sc_multi_wires:
    Name: Copper wire cross-sectional area of a sinlge wire
    Description: The cross-sectional area of a single copper conductor
    Units: mm²

   # sc_two_wires:
   #  Name: 2-wire copper cross-sectional area
   #  Description: The cross-sectional area of the copper conductor used in the coils based on commercial wire sizes and using two parallel wires at hand
   #  Units: mm2

   #
   # sc_two_wires_theor:
   #  Name: 2-wire copper cross-sectional area (Calculated)
   #  Description: The calculated cross-sectional area of the copper conductor used in the coils using two parallel wires at hand
   #  Units: mm2

  dc_theor:
    Name: Copper wire diameter (Calculated)
    Description: The calculated diameter of the copper conductor
    Units: mm
  wc:
    Name: Coil leg width
    Description: The width of the coil leg
    Units: mm

  lavg:
    Name: Average length of coil turn
    Description: Average length of one coil turn
    Units: mm

  tc_real:
    Name: Stator nominal temperature
    Description: Stator operating temperature at the nominal wind speed
    Units: C°

  stator_surface_area:
    Name: Stator surface area
    Description: The surface area of all stator coils
    Units: cm²

  cq_real:
    Name: Heat coefficient
    Description: The value of the heat coefficient after calculating the coil surface area and the nominal current
    Units:

  Jmax_real:
    Name: Current density
    Description: The current density at the nominal wind speed
    Units: A

  pt:
    Name: Resistivity of copper at nominal temperature
    Description: The resistivity of copper during nominal temperature in the stator, reached at nominal wind speed
    Units: Ohm.m

  Rc:
    Name: Resistance of a single coil
    Description: Resistance of a sinlge coil
    Units: Ohm

  Rphase:
    Name: Phase resistance
    Description: The resistance of a single phase group
    Units: Ohm

  Ls:
    Name: Phase inductance
    Description: Phase inductance
    Units: H

  Kn:
    Name: Nagaoka constant
    Description: The Nagaoka constant for the calculation of phase inductance
    Units:

  kw:
    Name: Winding coefficient
    Description: A winding coefficient which takes the value of 0.95 for rectangular and keyhole coils, but for triangular coils takes smaller values in order to compensate for the smaller coil hole.
    Units:

  coil_volume:
    Name: Volume of a single coil
    Description: Volume of a single coil
    Units: mm3

  coil_weight_constr:
    Name: Weight of a single coil
    Description: Weight of a single coil
    Units: g

  coil_hole_Ravg:
    Name: Coil hole at Ravg
    Description: Coil hole width at the average radius of the effective length of the generator
    Units: mm

  coil_hole_Rin_constr:
    Name: Coil hole at Rin
    Description: Coil hole width at the inner radius of the effective length of the generator, as to be used in the coil winder
    Units: mm

  coil_hole_Rout_constr:
    Name: Coil hole at Rout
    Description: Coil hole width at the outer radius of the effective length of the generator, as to be used in the coil winder
    Units: mm

  mcu_constr:
    Name: Coil mass for construction
    Description: Total copper mass for coils including two extra coils for contingency
    Units: kgr

  FluxRmsA:
    Name: RMS flux linkage of phase A
    Description: The average RMS value of the flux linkage of phase A at the nominal wind speed
    Units: Wb

  FluxRmsB:
    Name: RMS flux linkage of phase B
    Description: The average RMS value of the flux linkage of phase B at the nominal wind speed
    Units: Wb

  FluxRmsC:
    Name: RMS flux linkage of phase C
    Description: The average RMS value of the flux linkage of phase C at the nominal wind speed
    Units: Wb

  EMF_cut_in:
    Name: EMF at the cut-in wind speed (Calculated)
    Description: The calculated RMS value of the average EMF of all phases at the cut-in wind speed using analytical formulas
    Units: V

  # EMF_inst:
  #   Name: EMF at the instantaneous wind speed
  #   Description: The calculated RMS value of the average EMF of all phases at the instantaneous wind speed
  #   Units: V

  EMF_nom:
    Name: EMF at the nominal wind speed (Calculated)
    Description: The calculated RMS value of the average EMF of all phases at the nominal wind speed using analytical formulas
    Units: V

  V_rmsA:
    Name: RMS of EMF of phase A (Flux Linkage)
    Description: The RMS value of the EMF voltage for phase A at the nominal wind speed from FEMM simulated values of flux linkage using the method of differences for differentiation
    Units: V

  V_rmsB:
    Name: RMS of EMF of phase B (Flux Linkage)
    Description: The RMS value of the EMF voltage for phase B at the nominal wind speed from FEMM simulated values of flux linkage using the method of differences for differentiation
    Units: V

  V_rmsC:
    Name: RMS of EMF of phase C (Flux Linkage)
    Description: The RMS value of the EMF voltage for phase C at the nominal wind speed from FEMM simulated values of flux linkage using the method of differences for differentiation
    Units: V

  # Iac_max:
  #   Name: Current at the instantaneous wind speed
  #   Description: The calculated RMS value of the average line current of all phases at the instantaneous wind speed
  #   Units: A

  Iac_nom:
    Name: Current at the nominal wind speed
    Description: The calculated RMS value of the average line current of all phases at the nominal wind speed
    Units: A

  V_term_line:
    Name: Terminal voltage at the nominal wind speed
    Description: The terminal phase voltage of the generator
    Units: V
  # Jmax:
  #   Name: Current density
  #   Description: The maximum current density at the instantaneous wind speed
  #   Units: A/mm2

  fnom:
    Name: Nominal frequency
    Description: The frequency of the voltage and the current of the generator at nominal wind speed and RPM
    Units: Hz

  Pnom:
    Name: Generator power (Calculated)
    Description: The calculated power produced by the generator at the nominal wind speed using analytical formulas
    Units: W

  PelN_Torque_x:
    Name: Generator power (Torque)
    Description: The power produced by the generator at the nominal wind speed using simulated values of force from FEMM
    Units: W

  # Pnom_inst:
  #   Name: Generator instantaneous power (Calculated)
  #   Description: The calculated instantaneous power produced by the generator at the instantaneous wind speed
  #   Units: W

  PHMN_Torque_x:
    Name: Electromagnetic power (Torque)
    Description: The electromagnetic power produced by the generator at the nominal wind speed using simulated values of force from FEMM
    Units: W

  Pcopper:
    Name: Copper losses (Calculated)
    Description: The calculated copper losses in the stator at the nominal wind speed using analytical formulas
    Units: W

  PcopperN_Torque_x:
    Name: Copper losses (Torque)
    Description: Copper losses in the stator at the nominal wind speed using simulated values of force from FEMM
    Units: W

  Peddy:
    Name: Eddy current losses (Calculated)
    Description: The calculated eddy current losses in the stator at the nominal wind speed using analytical formulas
    Units: W

  PeddyN_Torque_x:
    Name: Eddy current losses (Torque)
    Description: Eddy current losses in the stator at the nominal wind speed using simulated values of force from FEMM
    Units: W

  efftotalN_Torque_x:
    Name: Generator efficiency (Torque)
    Description: The total efficiency of the generator using simulated values of Force from FEMM
    Units:

  efficiency_real:
    Name: Generator efficiency (Calculated)
    Description: The calculated total efficiency of the generator using analytical formulas
    Units:

  Pnom_rect:
    Name: AC Power at the rectifier (Calculated)
    Description: The calculated AC power arriving at the rectifier at the nominal wind speed, including losses on the power transmission cables from the wind turbine to the rectifier using analytical formulas
    Units: W

  PelN_Torque_x_rect:
    Name: AC Power at the rectifier (Torque)
    Description: The AC power before the rectifier at the nominal wind speed using simulated values of force from FEMM, including 20% losses on the power transmission cables from the wind turbine to the rectifier
    Units: W

  Pnom_dc:
    Name: DC Power at the batteries (Calculated)
    Description: The calculated DC power after the rectifier at the nominal wind speed including losses at the rectifier using analytical formulas
    Units: W

  PelN_Torque_x_dc:
    Name: DC Power at the batteries (Torque)
    Description: The DC power after the rectifier which is fed into the batteries at the nominal wind speed using simulated values of force from FEMM and including 10% losses at the rectifier
    Units: W



Generator Bearing Hub Outputs:

  No_bolts_hub :
    Name: Number of bolts on hub
    Description: The number of bolts to be used on the bearing hub
    Units:

  BearingHubMass:
    Name: Mass of bearing hub
    Description: The mass of the bearing hub
    Units: g

  Prot:
    Name: Rotational losses (Calculated)
    Description: The calculated frictional losses on the bearing hub due to the rotation of the rotor at the nominal wind speed using analytical formulas
    Units: W

  ProtN_Torque_x:
    Name: Rotational losses (Torque)
    Description: The frictional losses on the bearing hub due to rotation of the rotorat the nominal wind speed using simulated values of force from FEMM
    Units: W

Costs, Mass and Volume:

  Total_Mass_out:
    Name: Generator mass
    Description: The mass of the generator during the last design iteration for each time the optimization was repeated
    Units: kg
  Total_Cost_out:
    Name: Generator cost
    Description: The cost of the generator during the last design iteration for each time the optimization was repeated
    Units: €

  Totalcost:
    Name: Total generator cost
    Description: Total generator cost
    Units: EUR

  TotalMass:
    Name: Total generator mass
    Description: Total generator mass
    Units: kgr

  TotalVolume:
    Name: Total generator volume
    Description: Total generator volume
    Units: dm3

  blade_rotor_mass:
    Name: Blade rotor mass
    Description: Estimation of the mass of the three pinewood  rotor blades
    Units: kgr

  softwood_density:
    Name: Softwood density
    Description: The density of the soft wood used to make the blades, usually pine
    Units: kgr/m³

  Magcost:
    Name: Cost of magnets
    Description: Total cost of all magnets on rotors including shipping costs
    Units: EUR

  shippingcost:
    Name: Magnet shipping cost
    Description: The shipping cost for all magnets in the rotors based on volume
    Units: EUR

  mcu:
    Name: Copper mass
    Description: Total copper mass of coils in stator
    Units: kgr

  coppercost:
    Name: Cost of copper
    Description: The cost of copper for all stator coils
    Units: EUR

  resinmass:
    Name: Resin mass
    Description: The total mass of resin in the stator and rotors
    Units: kgr

  resincost:
    Name: Cost of resin
    Description: The total cost of resin in the stator and rotors
    Units: EUR

  ResinVolume:
    Name: Resin volume
    Description: The total volume of resin in the stator and rotors
    Units: cm3

  TotalMassRotor:
    Name: Total mass of one generator rotor
    Description: Total mass of one generator rotor including disk and magnets
    Units: kgr

  ironcost:
    Name: Cost of iron
    Description: The cost of iron for the rotor disks
    Units: EUR

  woodS:
    Name: Surface area of moulds
    Description: Estimation of the surface area of the plywood moulds
    Units: m2

  woodcost:
    Name: Total cost of moulds
    Description: Plywood for the construction of stator and rotor moulds
    Units: EUR
